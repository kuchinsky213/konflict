package com.ggs.Glaz.Act;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import com.ggs.Glaz.R;

public class One extends AppCompatActivity {
Button back;
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);
        back = findViewById(R.id.btnBack);

        back.setOnClickListener(v->{
            onBackPressed();
            finish();
        });



    }
}