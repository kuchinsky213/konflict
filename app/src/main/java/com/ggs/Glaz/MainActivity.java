package com.ggs.Glaz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;


import com.ggs.Glaz.Act.Five;
import com.ggs.Glaz.Act.Four;
import com.ggs.Glaz.Act.One;
import com.ggs.Glaz.Act.Six;
import com.ggs.Glaz.Act.Three;
import com.ggs.Glaz.Act.Two;

public class MainActivity extends AppCompatActivity {
CardView one, two, three, four, five, six;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        four = findViewById(R.id.four);
        five = findViewById(R.id.five);
        six = findViewById(R.id.six);

        one.setOnClickListener(v ->{
            startActivity(new Intent(MainActivity.this, One.class));
        });

        two.setOnClickListener(v->{
            startActivity(new Intent(MainActivity.this, Two.class));
        });
        three.setOnClickListener(v->{
            startActivity(new Intent(MainActivity.this, Three.class));
        });

        four.setOnClickListener(v->{
            startActivity(new Intent(MainActivity.this, Four.class));
        });

        five.setOnClickListener(v->{
            startActivity(new Intent(MainActivity.this, Five.class));
        });


        six.setOnClickListener(v->{
            startActivity(new Intent(MainActivity.this, Six.class));
        });



    }
}